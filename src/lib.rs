#![no_std]

#[cfg(feature = "alloc")]
extern crate alloc;

mod glyph;
mod unifont;

#[cfg(test)]
mod testutil;

pub use glyph::Glyph;
pub use unifont::get_glyph;
#[cfg(feature = "alloc")]
pub use unifont::enumerate_glyphs;
pub use unifont::enumerate_glyphs_without_allocating;
pub use unifont::get_storage_size;
