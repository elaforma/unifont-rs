use core::char::from_u32_unchecked;
use core::mem::size_of_val;
use glyph::Glyph;

pub fn get_glyph(c: char) -> Option<&'static Glyph> {
    let code_point = c as usize;
    let mut offset: usize = 0;
    let mut result = None;
    for (start, end) in CODE_POINT_RANGES.iter() {
         if *start <= code_point && code_point < *end {
             result = Some(&GLYPH_TABLE[offset + code_point - start]);
             break;
         } else {
             offset += end - start;
         }
    }
    result
}

#[cfg(feature = "alloc")]
pub fn enumerate_glyphs() -> alloc::boxed::Box<dyn Iterator<Item=(char, &'static Glyph)>> {
    alloc::boxed::Box::new(enumerate_glyphs_without_allocating())
}

pub fn enumerate_glyphs_without_allocating() -> impl Iterator<Item=(char, &'static Glyph)> {
    let char_iterator = CODE_POINT_RANGES.iter()
        .flat_map(|(start, end)| *start..*end)
        .map(|code_point| unsafe { from_u32_unchecked(code_point as u32) });
    let glyph_iterator = GLYPH_TABLE.iter();
    char_iterator.zip(glyph_iterator)
}

pub fn get_storage_size() -> usize {
    size_of_val(&CODE_POINT_RANGES) + size_of_val(&GLYPH_TABLE)
}

include!(concat!(env!("OUT_DIR"), "/glyph_table.rs"));

#[cfg(test)]
mod tests {
    use testutil;
    use super::*;

    #[test]
    fn glyph_a() {
        let glyph = get_glyph('a').unwrap();
        assert_eq!(glyph, &testutil::GLYPH_A);
    }

    #[test]
    fn glyph_ji() {
        let glyph = get_glyph('字').unwrap();
        assert_eq!(glyph, &testutil::GLYPH_JI);
    }

    #[test]
    #[cfg(feature = "alloc")]
    fn enumeration_old() {
        let glyph_a = get_glyph('a').unwrap();
        let glyph_ji = get_glyph('字').unwrap();
        for (c, glyph) in enumerate_glyphs() {
            match c {
                'a' => assert_eq!(glyph, glyph_a),
                '字' => assert_eq!(glyph, glyph_ji),
                _ => {},
            }
        }
    }

    #[test]
    fn enumeration_new() {
        let glyph_a = get_glyph('a').unwrap();
        let glyph_ji = get_glyph('字').unwrap();
        for (c, glyph) in enumerate_glyphs_without_allocating() {
            match c {
                'a' => assert_eq!(glyph, glyph_a),
                '字' => assert_eq!(glyph, glyph_ji),
                _ => {},
            }
        }
    }
}
